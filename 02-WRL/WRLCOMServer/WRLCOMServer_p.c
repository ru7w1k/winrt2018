

/* this ALWAYS GENERATED file contains the proxy stub code */


 /* File created by MIDL compiler version 8.00.0603 */
/* at Tue Dec 04 19:59:54 2018
 */
/* Compiler settings for C:\Users\rutwi\AppData\Local\Temp\WRLCOMServer.idl-89659557:
    Oicf, W1, Zp8, env=Win32 (32b run), target_arch=ARM 8.00.0603 
    protocol : dce , ms_ext, c_ext, robust
    error checks: allocation ref bounds_check enum stub_data 
    VC __declspec() decoration level: 
         __declspec(uuid()), __declspec(selectany), __declspec(novtable)
         DECLSPEC_UUID(), MIDL_INTERFACE()
*/
/* @@MIDL_FILE_HEADING(  ) */

#if defined(_ARM_)


#pragma warning( disable: 4049 )  /* more than 64k source lines */
#if _MSC_VER >= 1200
#pragma warning(push)
#endif

#pragma warning( disable: 4211 )  /* redefine extern to static */
#pragma warning( disable: 4232 )  /* dllimport identity*/
#pragma warning( disable: 4024 )  /* array to pointer mapping*/
#pragma warning( disable: 4152 )  /* function/data pointer conversion in expression */

#define USE_STUBLESS_PROXY


/* verify that the <rpcproxy.h> version is high enough to compile this file*/
#ifndef __REDQ_RPCPROXY_H_VERSION__
#define __REQUIRED_RPCPROXY_H_VERSION__ 475
#endif


#include "rpcproxy.h"
#ifndef __RPCPROXY_H_VERSION__
#error this stub requires an updated version of <rpcproxy.h>
#endif /* __RPCPROXY_H_VERSION__ */


#include "WRLCOMServer_h.h"

#define TYPE_FORMAT_STRING_SIZE   7                                 
#define PROC_FORMAT_STRING_SIZE   57                                
#define EXPR_FORMAT_STRING_SIZE   1                                 
#define TRANSMIT_AS_TABLE_SIZE    0            
#define WIRE_MARSHAL_TABLE_SIZE   0            

typedef struct _WRLCOMServer_MIDL_TYPE_FORMAT_STRING
    {
    short          Pad;
    unsigned char  Format[ TYPE_FORMAT_STRING_SIZE ];
    } WRLCOMServer_MIDL_TYPE_FORMAT_STRING;

typedef struct _WRLCOMServer_MIDL_PROC_FORMAT_STRING
    {
    short          Pad;
    unsigned char  Format[ PROC_FORMAT_STRING_SIZE ];
    } WRLCOMServer_MIDL_PROC_FORMAT_STRING;

typedef struct _WRLCOMServer_MIDL_EXPR_FORMAT_STRING
    {
    long          Pad;
    unsigned char  Format[ EXPR_FORMAT_STRING_SIZE ];
    } WRLCOMServer_MIDL_EXPR_FORMAT_STRING;


static const RPC_SYNTAX_IDENTIFIER  _RpcTransferSyntax = 
{{0x8A885D04,0x1CEB,0x11C9,{0x9F,0xE8,0x08,0x00,0x2B,0x10,0x48,0x60}},{2,0}};


extern const WRLCOMServer_MIDL_TYPE_FORMAT_STRING WRLCOMServer__MIDL_TypeFormatString;
extern const WRLCOMServer_MIDL_PROC_FORMAT_STRING WRLCOMServer__MIDL_ProcFormatString;
extern const WRLCOMServer_MIDL_EXPR_FORMAT_STRING WRLCOMServer__MIDL_ExprFormatString;


extern const MIDL_STUB_DESC Object_StubDesc;


extern const MIDL_SERVER_INFO __x_ABI_CWRLCOMServer_CISum_ServerInfo;
extern const MIDL_STUBLESS_PROXY_INFO __x_ABI_CWRLCOMServer_CISum_ProxyInfo;


extern const MIDL_STUB_DESC Object_StubDesc;


extern const MIDL_SERVER_INFO __x_ABI_CWRLCOMServer_CISubtract_ServerInfo;
extern const MIDL_STUBLESS_PROXY_INFO __x_ABI_CWRLCOMServer_CISubtract_ProxyInfo;



#if !defined(__RPC_ARM32__)
#error  Invalid build platform for this stub.
#endif

#if !(TARGET_IS_NT50_OR_LATER)
#error You need Windows 2000 or later to run this stub because it uses these features:
#error   /robust command line switch.
#error However, your C/C++ compilation flags indicate you intend to run this app on earlier systems.
#error This app will fail with the RPC_X_WRONG_STUB_VERSION error.
#endif


static const WRLCOMServer_MIDL_PROC_FORMAT_STRING WRLCOMServer__MIDL_ProcFormatString =
    {
        0,
        {

	/* Procedure SubtractionOfTwoIntegers */


	/* Procedure SumOfTwoIntegers */

			0x33,		/* FC_AUTO_HANDLE */
			0x6c,		/* Old Flags:  object, Oi2 */
/*  2 */	NdrFcLong( 0x0 ),	/* 0 */
/*  6 */	NdrFcShort( 0x6 ),	/* 6 */
/*  8 */	NdrFcShort( 0x14 ),	/* ARM Stack size/offset = 20 */
/* 10 */	NdrFcShort( 0x10 ),	/* 16 */
/* 12 */	NdrFcShort( 0x24 ),	/* 36 */
/* 14 */	0x44,		/* Oi2 Flags:  has return, has ext, */
			0x4,		/* 4 */
/* 16 */	0x10,		/* 16 */
			0x1,		/* Ext Flags:  new corr desc, */
/* 18 */	NdrFcShort( 0x0 ),	/* 0 */
/* 20 */	NdrFcShort( 0x0 ),	/* 0 */
/* 22 */	NdrFcShort( 0x0 ),	/* 0 */
/* 24 */	NdrFcShort( 0x4 ),	/* 4 */
/* 26 */	0x4,		/* 4 */
			0x80,		/* 128 */
/* 28 */	0x81,		/* 129 */
			0x82,		/* 130 */
/* 30 */	0x83,		/* 131 */
			0x0,		/* 0 */

	/* Parameter a */


	/* Parameter a */

/* 32 */	NdrFcShort( 0x48 ),	/* Flags:  in, base type, */
/* 34 */	NdrFcShort( 0x4 ),	/* ARM Stack size/offset = 4 */
/* 36 */	0x8,		/* FC_LONG */
			0x0,		/* 0 */

	/* Parameter b */


	/* Parameter b */

/* 38 */	NdrFcShort( 0x48 ),	/* Flags:  in, base type, */
/* 40 */	NdrFcShort( 0x8 ),	/* ARM Stack size/offset = 8 */
/* 42 */	0x8,		/* FC_LONG */
			0x0,		/* 0 */

	/* Parameter value */


	/* Parameter value */

/* 44 */	NdrFcShort( 0x2150 ),	/* Flags:  out, base type, simple ref, srv alloc size=8 */
/* 46 */	NdrFcShort( 0xc ),	/* ARM Stack size/offset = 12 */
/* 48 */	0x8,		/* FC_LONG */
			0x0,		/* 0 */

	/* Return value */


	/* Return value */

/* 50 */	NdrFcShort( 0x70 ),	/* Flags:  out, return, base type, */
/* 52 */	NdrFcShort( 0x10 ),	/* ARM Stack size/offset = 16 */
/* 54 */	0x8,		/* FC_LONG */
			0x0,		/* 0 */

			0x0
        }
    };

static const WRLCOMServer_MIDL_TYPE_FORMAT_STRING WRLCOMServer__MIDL_TypeFormatString =
    {
        0,
        {
			NdrFcShort( 0x0 ),	/* 0 */
/*  2 */	
			0x11, 0xc,	/* FC_RP [alloced_on_stack] [simple_pointer] */
/*  4 */	0x8,		/* FC_LONG */
			0x5c,		/* FC_PAD */

			0x0
        }
    };


/* Standard interface: __MIDL_itf_WRLCOMServer_0000_0000, ver. 0.0,
   GUID={0x00000000,0x0000,0x0000,{0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00}} */


/* Object interface: IUnknown, ver. 0.0,
   GUID={0x00000000,0x0000,0x0000,{0xC0,0x00,0x00,0x00,0x00,0x00,0x00,0x46}} */


/* Object interface: IInspectable, ver. 0.0,
   GUID={0xAF86E2E0,0xB12D,0x4c6a,{0x9C,0x5A,0xD7,0xAA,0x65,0x10,0x1E,0x90}} */


/* Object interface: __x_ABI_CWRLCOMServer_CISum, ver. 0.0,
   GUID={0x0be9429f,0x2c7a,0x40e8,{0xbb,0x0a,0x85,0xbc,0xb1,0x74,0x93,0x67}} */

#pragma code_seg(".orpc")
static const unsigned short __x_ABI_CWRLCOMServer_CISum_FormatStringOffsetTable[] =
    {
    (unsigned short) -1,
    (unsigned short) -1,
    (unsigned short) -1,
    0
    };

static const MIDL_STUBLESS_PROXY_INFO __x_ABI_CWRLCOMServer_CISum_ProxyInfo =
    {
    &Object_StubDesc,
    WRLCOMServer__MIDL_ProcFormatString.Format,
    &__x_ABI_CWRLCOMServer_CISum_FormatStringOffsetTable[-3],
    0,
    0,
    0
    };


static const MIDL_SERVER_INFO __x_ABI_CWRLCOMServer_CISum_ServerInfo = 
    {
    &Object_StubDesc,
    0,
    WRLCOMServer__MIDL_ProcFormatString.Format,
    &__x_ABI_CWRLCOMServer_CISum_FormatStringOffsetTable[-3],
    0,
    0,
    0,
    0};
CINTERFACE_PROXY_VTABLE(7) ___x_ABI_CWRLCOMServer_CISumProxyVtbl = 
{
    &__x_ABI_CWRLCOMServer_CISum_ProxyInfo,
    &IID___x_ABI_CWRLCOMServer_CISum,
    IUnknown_QueryInterface_Proxy,
    IUnknown_AddRef_Proxy,
    IUnknown_Release_Proxy ,
    0 /* IInspectable::GetIids */ ,
    0 /* IInspectable::GetRuntimeClassName */ ,
    0 /* IInspectable::GetTrustLevel */ ,
    (void *) (INT_PTR) -1 /* __x_ABI_CWRLCOMServer_CISum::SumOfTwoIntegers */
};


static const PRPC_STUB_FUNCTION __x_ABI_CWRLCOMServer_CISum_table[] =
{
    STUB_FORWARDING_FUNCTION,
    STUB_FORWARDING_FUNCTION,
    STUB_FORWARDING_FUNCTION,
    NdrStubCall2
};

CInterfaceStubVtbl ___x_ABI_CWRLCOMServer_CISumStubVtbl =
{
    &IID___x_ABI_CWRLCOMServer_CISum,
    &__x_ABI_CWRLCOMServer_CISum_ServerInfo,
    7,
    &__x_ABI_CWRLCOMServer_CISum_table[-3],
    CStdStubBuffer_DELEGATING_METHODS
};


/* Standard interface: __MIDL_itf_WRLCOMServer_0000_0001, ver. 0.0,
   GUID={0x00000000,0x0000,0x0000,{0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00}} */


/* Object interface: __x_ABI_CWRLCOMServer_CISubtract, ver. 0.0,
   GUID={0xCD3DFDF1,0xA779,0x4E30,{0xB3,0x45,0x52,0x81,0xC4,0x0E,0x10,0xF4}} */

#pragma code_seg(".orpc")
static const unsigned short __x_ABI_CWRLCOMServer_CISubtract_FormatStringOffsetTable[] =
    {
    (unsigned short) -1,
    (unsigned short) -1,
    (unsigned short) -1,
    0
    };

static const MIDL_STUBLESS_PROXY_INFO __x_ABI_CWRLCOMServer_CISubtract_ProxyInfo =
    {
    &Object_StubDesc,
    WRLCOMServer__MIDL_ProcFormatString.Format,
    &__x_ABI_CWRLCOMServer_CISubtract_FormatStringOffsetTable[-3],
    0,
    0,
    0
    };


static const MIDL_SERVER_INFO __x_ABI_CWRLCOMServer_CISubtract_ServerInfo = 
    {
    &Object_StubDesc,
    0,
    WRLCOMServer__MIDL_ProcFormatString.Format,
    &__x_ABI_CWRLCOMServer_CISubtract_FormatStringOffsetTable[-3],
    0,
    0,
    0,
    0};
CINTERFACE_PROXY_VTABLE(7) ___x_ABI_CWRLCOMServer_CISubtractProxyVtbl = 
{
    &__x_ABI_CWRLCOMServer_CISubtract_ProxyInfo,
    &IID___x_ABI_CWRLCOMServer_CISubtract,
    IUnknown_QueryInterface_Proxy,
    IUnknown_AddRef_Proxy,
    IUnknown_Release_Proxy ,
    0 /* IInspectable::GetIids */ ,
    0 /* IInspectable::GetRuntimeClassName */ ,
    0 /* IInspectable::GetTrustLevel */ ,
    (void *) (INT_PTR) -1 /* __x_ABI_CWRLCOMServer_CISubtract::SubtractionOfTwoIntegers */
};


static const PRPC_STUB_FUNCTION __x_ABI_CWRLCOMServer_CISubtract_table[] =
{
    STUB_FORWARDING_FUNCTION,
    STUB_FORWARDING_FUNCTION,
    STUB_FORWARDING_FUNCTION,
    NdrStubCall2
};

CInterfaceStubVtbl ___x_ABI_CWRLCOMServer_CISubtractStubVtbl =
{
    &IID___x_ABI_CWRLCOMServer_CISubtract,
    &__x_ABI_CWRLCOMServer_CISubtract_ServerInfo,
    7,
    &__x_ABI_CWRLCOMServer_CISubtract_table[-3],
    CStdStubBuffer_DELEGATING_METHODS
};


/* Standard interface: __MIDL_itf_WRLCOMServer_0000_0002, ver. 0.0,
   GUID={0x00000000,0x0000,0x0000,{0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00}} */

static const MIDL_STUB_DESC Object_StubDesc = 
    {
    0,
    NdrOleAllocate,
    NdrOleFree,
    0,
    0,
    0,
    0,
    0,
    WRLCOMServer__MIDL_TypeFormatString.Format,
    1, /* -error bounds_check flag */
    0x50002, /* Ndr library version */
    0,
    0x800025b, /* MIDL Version 8.0.603 */
    0,
    0,
    0,  /* notify & notify_flag routine table */
    0x1, /* MIDL flag */
    0, /* cs routines */
    0,   /* proxy/server info */
    0
    };

const CInterfaceProxyVtbl * const _WRLCOMServer_ProxyVtblList[] = 
{
    ( CInterfaceProxyVtbl *) &___x_ABI_CWRLCOMServer_CISumProxyVtbl,
    ( CInterfaceProxyVtbl *) &___x_ABI_CWRLCOMServer_CISubtractProxyVtbl,
    0
};

const CInterfaceStubVtbl * const _WRLCOMServer_StubVtblList[] = 
{
    ( CInterfaceStubVtbl *) &___x_ABI_CWRLCOMServer_CISumStubVtbl,
    ( CInterfaceStubVtbl *) &___x_ABI_CWRLCOMServer_CISubtractStubVtbl,
    0
};

PCInterfaceName const _WRLCOMServer_InterfaceNamesList[] = 
{
    "__x_ABI_CWRLCOMServer_CISum",
    "__x_ABI_CWRLCOMServer_CISubtract",
    0
};

const IID *  const _WRLCOMServer_BaseIIDList[] = 
{
    &IID_IInspectable,
    &IID_IInspectable,
    0
};


#define _WRLCOMServer_CHECK_IID(n)	IID_GENERIC_CHECK_IID( _WRLCOMServer, pIID, n)

int __stdcall _WRLCOMServer_IID_Lookup( const IID * pIID, int * pIndex )
{
    IID_BS_LOOKUP_SETUP

    IID_BS_LOOKUP_INITIAL_TEST( _WRLCOMServer, 2, 1 )
    IID_BS_LOOKUP_RETURN_RESULT( _WRLCOMServer, 2, *pIndex )
    
}

const ExtendedProxyFileInfo WRLCOMServer_ProxyFileInfo = 
{
    (PCInterfaceProxyVtblList *) & _WRLCOMServer_ProxyVtblList,
    (PCInterfaceStubVtblList *) & _WRLCOMServer_StubVtblList,
    (const PCInterfaceName * ) & _WRLCOMServer_InterfaceNamesList,
    (const IID ** ) & _WRLCOMServer_BaseIIDList,
    & _WRLCOMServer_IID_Lookup, 
    2,
    2,
    0, /* table of [async_uuid] interfaces */
    0, /* Filler1 */
    0, /* Filler2 */
    0  /* Filler3 */
};
#if _MSC_VER >= 1200
#pragma warning(pop)
#endif


#endif /* if defined(_ARM_) */

